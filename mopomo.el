;;; mopomo.el --- Pomodoro package for emacs

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Marcos G Moritz <marcosmoritz@gmail.com>
;; Version: 0.0.1
;; Package-Requires: ((org "9.3.6"))
;; Keywords: pomodoro, productivity
;; URL: https://mo-profile.com

;;; Commentary:
;; This is package to handle pomodoro cycles

;; This is package to handle pomodoro cycles
;; creates an alert to help you manage your time
;; The pomodoro technique is a way to time box
;; blocks of work and set regular intervals
;; between them.

(require 'org)

(defvar mopomo--count 0)
(defvar mopomo--active nil)
(defvar mopomo--running nil)
(defvar mopomo--timer nil)
(defvar mopomo--start-time)
(defconst mopomo--log-file (substitute-in-file-name "$HOME/.mopomo_history"))

(defun mopomo (alert)
  (interactive "p")
  (if mopomo--running
      (mopomo-stop)
    (mopomo-start alert)))

(defun mopomo-start (alert)
  (interactive "p")
  (let ((length-minutes 25) ;; 25
        (short-interval 5)
        (long-interval 15)
        (block-length 4))
    (if (not mopomo--running)
        (progn
          (setq mopomo--running t)
          (mopomo-create-next length-minutes
                              short-interval
                              long-interval
                              block-length
                              nil
                              (not (= alert 4))))
      (message "mopomo is running"))))

(defun mopomo-create-next (length-minutes
                           short-interval
                           long-interval
                           block-length
                           intervalp
                           alert)
  (setq mopomo--active (not intervalp))
  (if (not intervalp)
      (setq mopomo--count (mod (+ 1 mopomo--count) block-length)))
  (let* ((mopomo-message "")
         (length-seconds
          (cond
           ((and intervalp (= mopomo--count 0))
            (progn
              (setq mopomo-message "long interval started")
              (mopomo--minutes-to-seconds long-interval)))
           ((and intervalp (> mopomo--count 0))
            (progn
              (setq mopomo-message "short interval started")
              (mopomo--minutes-to-seconds short-interval)))
           (t (progn
                (setq mopomo-message
                      (format "mopomo at %s"
                              (substring-no-properties org-clock-current-task)))
                (mopomo--minutes-to-seconds length-minutes))))))
    (mopomo--write-log mopomo-message alert)
    (setq mopomo--start-time (current-time))
    (setq mopomo--timer
          (run-at-time
           length-seconds nil 'mopomo-create-next
           length-minutes short-interval long-interval
           block-length (not intervalp) alert))))

(defun mopomo-stop ()
  (interactive)
  (setq mopomo--running nil)
  (let ((mopomo-message "mopomo stopped"))
    (if mopomo--active
        (mopomo--write-log mopomo-message)))
  (setq mopomo--active nil)
  (cancel-timer mopomo--timer)
  (org-clock-jump-to-current-clock))

(defun mopomo--minutes-to-seconds (minutes)
  (* 60 minutes))

(defun mopomo--write-log (mopomo-message &optional alert)
  (if alert
      (mopomo--alert mopomo-message)
    (message mopomo-message))
  (shell-command (format "echo %s: '%s' >> %s"
                         (format-time-string "%d/%m/%y %H:%M:%S")
                         mopomo-message mopomo--log-file)))

(defun mopomo--alert (mopomo-message)
  (play-sound-file "/usr/share/sounds/speech-dispatcher/test.wav")
  (message-box mopomo-message))

;; (advice-add 'org-clock-in :after #'mopomo-start)
;; (advice-add 'org-clock-out :before #'mopomo-stop)

(provide 'mopomo)
