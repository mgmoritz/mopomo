# What it does

Manages pomodoro times and create a time with pomodoro history.

# Install

```elisp
(use-package mopomo
  :if (file-directory-p "/path/to/lib/mopomo")
  :load-path "/path/to/lib/mopomo")
```

# Usage

Clock in to a org-mode task and call `M-x mopomo`.
